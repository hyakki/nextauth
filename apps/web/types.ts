type Collection = {
  id: number
  name: string
}

type Collections = Collection[]
