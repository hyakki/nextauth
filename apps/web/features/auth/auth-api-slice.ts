import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

type LoginResponse = {
  access_token: string
  expires: number
  refresh_token: string
}

type Credentials = {
  email: string
  password: string
}

// Define a service using a base URL and expected endpoints
export const api = createApi({
  reducerPath: 'authsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:4000/auth',
    // prepareHeaders: (headers, { getState }) => {
    //   const u = (getState() as any).auth
    //   console.log(u)

    //   headers.set('Content-type', 'application/json')
    //   return headers
    // },
  }),

  endpoints: builder => ({
    login: builder.mutation<LoginResponse, Credentials>({
      query: credentials => ({
        url: 'login',
        method: 'POST',
        body: JSON.stringify(credentials),
      }),
      transformResponse: (response: { data: LoginResponse }) => {
        return response.data
      },
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useLoginMutation } = api
