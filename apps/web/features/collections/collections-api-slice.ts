import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// Define a service using a base URL and expected endpoints
export const api = createApi({
  reducerPath: 'collectionsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:4000',
    // prepareHeaders: (headers, { getState }) => {
    //   const u = (getState() as any).auth
    //   console.log(u)

    //   headers.set('Content-type', 'application/json')
    //   return headers
    // },
  }),

  endpoints: builder => ({
    getCollections: builder.query<Collections, string>({
      query: token => {
        return {
          url: 'items/collections',
          headers: [
            ['Content-type', 'application/json'],
            ['Authorization', `Bearer ${token}`],
          ],
          params: [['fields', '*']],
        }
      },
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetCollectionsQuery } = api
