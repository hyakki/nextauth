import { useSession } from 'next-auth/react'
import { useGetCollectionsQuery } from '@/features/collections/collections-api-slice'

export default function Page() {
  const { data: session, status } = useSession({
    required: true,
  })
  const token = session?.user?.accessToken
  const {
    data: collections,
    isFetching,
    error,
  } = useGetCollectionsQuery(token, {
    skip: !token || status !== 'authenticated',
  })

  if (isFetching || status !== 'authenticated') return <div>Loading...</div>
  if (error) return <div>Failed to load {JSON.stringify(error)}</div>
  if (!collections) return <div>No collections found</div>

  return (
    <main>
      <pre>
        <code>{JSON.stringify(collections, null, 2)}</code>
      </pre>
    </main>
  )
}
