import { configureStore } from '@reduxjs/toolkit'
import { api as collectionsAPI } from '@/features/collections/collections-api-slice'
import { api as authAPI } from '@/features/auth/auth-api-slice'
import { setupListeners } from '@reduxjs/toolkit/query'

export const store = configureStore({
  reducer: {
    [collectionsAPI.reducerPath]: collectionsAPI.reducer,
    [authAPI.reducerPath]: authAPI.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      .concat(collectionsAPI.middleware)
      .concat(authAPI.middleware),
})

setupListeners(store.dispatch)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
